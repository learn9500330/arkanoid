
; =========================================================================================================
; =========================================== DOC =========================================================

; =========================================================================================================
; Beschreibung des Programms: =============================================================================
; Es handelt sich hierbei um eine Implementation eines Arkonnoid oder Bricks ähnlichen Spiels. Dieses wird
; auf einer 7x5 Punktmatrixanzeige dargestellt. Ferner gibt es noch eine Punkteanzeige für die Punkte des
; Spielers.
; Der Spieler in der untersten Reihe kann über die externen Interrupts Null und Eins nach rechts
; bzw. links verschoben werden.
; In den obersten beiden Reihen der insgesamt 7 Reihen befinden sich zu beginn Blöcke, die man zerstören
; kann indem man den Ball dagegen prallen lässt. Für jeden zerstörten Block erhält man Punkte.
; Der Spieler in der untersten Reihe ist zwei Blöcke breit. Wenn der Ball ihn trifft prallt er von ihm ab.
; Trifft er dabei den linken Block so wird seine Geschwindigkeit nach links hin um eins erhöht sofern er
; nicht bereits die maximal zulässige Geschwindigkeit von 1 Blöcken pro Zyklus in diese Richtung besitzt.
; Prallt er von dem rechten Block ab gilt das selbe in komplementärer Ausführung. Der Ball prallt nicht ab,
; wenn der schräg auf die Kante des Spielers trifft. Dies hat den Zweck das Spiel schwerer zu gestalten,
; somit die Fläche von der er abprallen kann reduziert wird. Ohne diese Maßnahme ergäbe sich die
; Möglichkeit den Spieler so zu plazieren, dass der Ball in jedem sich ergebenen Fall vom aus dem Spielfeld
; fliegen abgehalten wird. In der Vertikalen kann sich der Ball nur mit einer Geschwindigkeit von
; betragsmäßig 1 bewegen. Wenn der Spieler links oder rechts aus dem Spielfeld geht dan kommt er von der
; anderen Seite wieder rein. Man hat das Spiel dann verloren, wenn der Ball an der untersten Reihe vorbei
; aus dem Spielfeld fliegt. Von allen anderen Reihen Prallt der Ball ab. Zerstört man alle Blöcke kommt man
; in die nächste Runde und die Blöcke werden neu plaziert. Ferner erhöht sich mit jeder Runde die
; Geschwindigkeit des Spiels. Pro zerstörten Block bekommt man einen Punkt. Die Punkte werden mit zwei
; Ziffern im Hexadezimalsystem dargestellt. Dem entsprechend get es nur bis 256 und danach fängt es wieder
; von 0 an. Sollte man so weit kommen muss man sich halt merken, dass es übergelaufen ist.

; =========================================================================================================
; Parameter und deren Speicherort: ========================================================================
; @param: y-Richtung
;   - Vertikale Richtung, in die der Ball fliegt
;   - gespeichert in: 10h (bit)
;   -  1 = nach unten, 0 = nach oben
; @param: x-Geschwindigkeit
;   - Geschwindigkeit mit der sich der Ball in der Horizontalen bewegt
;   - gespeichert in: R5
;   - maximaler Betrag = 1
;   - bestimmt wohin sich der Ball auf der x-Achse als nächstes bewegt
;     negative heißt er bewegt sich nach links positive er bewegt sich nach rechts
;     er wird einfach auf die x-Position addiert
; @param: Reihe 0
;   - Speichert die Blöcke der obersten Blockreihe
;   - gespeichert in: 20h (dadr)
;   - die zugehörige Speicherstelle ist bitadressierbar ein eingeschaltetes Bit bedeutet, dass der
;     Zugehörige Block noch existiert; ein ausgeschaltetes, das er zerstört wurde
; @param: Reihe 1
;   - Speichert die Blöcke der unteren Blockreihe
;   - gespeichert in: 21h (dadr)
;   - die zugehörige Speicherstelle ist bitadressierbar ein eingeschaltetes Bit bedeutet, dass der
;     Zugehörige Block noch existiert; ein ausgeschaltetes, das er zerstört wurde
; @param: Spielerposition
;   - Speichert die Positionen der Blöcke des Spielers
;   - gespeichert in: R4
;   - der Spieler ist immer in der letzten Zeile daher muss nur die Ausrichtung es Spielers in der
;     Horizonten gespeichert werden
;   - es wird ein Bitmuster mit Einsen an den Stellen der Blöcke des Spielers gespeichert
;   - z.B. wenn #00011b gespeichert ist, dann bedeutet das, dass der Spieler sich mit dem rechten Block am
;     rechten Rand befindet und der linke Block ist hier durch das höhere Bit repräsentiert und somit links
;     vom anderen Block
; @param: Ball-x-Position
;   - Integer, das die x-Position des Balls bestimmt
;   - gespeichert in: R2
;   - x wächst nach rechts hin
; @param: Ball-y-Position
;   - Integer, das die y-Position des Balls bestimmt
;   - gespeichert in: R3
;   - y wächst nach unten hin
; @param: Punkte
;   - Punkte des Spielers
;   - gespeichert in: P2
;   - werden auch auf P2 angezeigt
;   - da sie in einem Bite geschieht werden geht es nur bis 255 danach get es bei 0 los
; @param: Game over
;   - speichert ob das Spiel läuft oder nicht
;   - gespeichert in: 11h (bit)
;   - 1 = läuft nicht; 0 = läuft
; @param: Spielgeschwindigkeit
;   - Geschwindigkeit mit der das Spiel abläuft
;   - gespeichert in: R6
;   - je niedriger desto schnelle sofern > 0
; @param: ist-laufschrift-blockiert
;   - das Bit hält fest ob das Anzeigen von Laufschrift blockiert wird
;   - gespeichert in: 12h (bit)
;   - wenn 1 wird laufschrift blockiert, beim Anzeigen von laufschrift werden erforderliche Unterprogramme
;     unterbrochen
; @param: Chartab-backup
;   - Backup für Tabelle für Spalten eines Zeichen, das auf der Punktmatrixanzeige angezeigt werden kann
;     (Zeichentabellen)
;   - gespeichert in: 23h-27h (dadr)

; R0 und R1 werden nicht zum speichern von Parametern genutzt weil es Befehle gibt, die
; nur mit diesen Registern funktionieren. Sie jedoch wie der Akku auch verwendet um
; zwischen Ergebnisse in Unterprogramm zu speichern. Auf diese weise wird auch so etwas
; wie Übergabeparameter und Rückgabewerte realisiert.

; =========================================================================================================
; Peripheriegeräte und deren Anschlüsse: ==================================================================
; |-------+----------------------------+------------------------|
; | Port  | Gerät                      | Funktion               |
; |-------+----------------------------+------------------------|
; | P0/P1 | 7*5 Punktmatrixanzeige     | Darstellung des Spiels |
; | P2    | 2*7-Segmentanzeige+Decoder | Punkteanzeige          |
; |-------+----------------------------+------------------------|
;
; =========================================================================================================
; wichtige programmspezifische Begriffe: ==================================================================
; |------------------+------------------------------------------------------------------------------------|
; | Begriff          | Bedeutung                                                                          |
; |------------------+------------------------------------------------------------------------------------|
; | Game-Over-Bit    | ein Bit, welches eins ist, wenn das Spiel nicht laufen soll                        |
; |------------------+------------------------------------------------------------------------------------|
; | Kollisionsmuster | Ein Bitmuster, welches an der Stelle, in der der Ball mit etwas kollidiert eine    |
; |                  | Eins hat, sofern er mit etwas kollidiert. Überall sonst ist es Null.               |
; |------------------+------------------------------------------------------------------------------------|
; | Spielfeld        | ein 7x5 Feld, in dem sich der Ball, der Spieler und die Blöcke aufhalten und das   |
; |                  | auf der Punktmatrixanzeige während des Spiels angezeigt wird                       |
; |------------------+------------------------------------------------------------------------------------|
; | Blockreihen      | Die oberen beiden Reihen, des Spielfelds, in den Blöcke sein können.               |
; |------------------+------------------------------------------------------------------------------------|
; | Zeichentabellen  | Eine Tabelle mit den fünf Bitmustern für die Spalten der Punktmatrixanzeige um ein |
; |                  | Zeichen darzustellen.                                                              |
; |------------------+------------------------------------------------------------------------------------|
; | game-over-screen | das, was auf der Punktmatrixanzeige angezeigt werden soll, wenn das Spiel vorbei   |
; |                  | ist                                                                                |
; |------------------+------------------------------------------------------------------------------------|
; | start-screen     | das, was auf der Punktmatrixanzeige angezeigt werden soll, wenn das Spiel noch     |
; |                  | nicht gestartet wurde                                                              |
; |------------------+------------------------------------------------------------------------------------|
;
; NOTE: 7x5 Punktmatrixanzeige: Spalte low-aktive, Zeile high-aktive (Spalte an P0 Zeile an P1)
; =========================================================================================================

include reg51.inc

JMP init
ORG 03h; ISR um Spieler nach rechts zu bewegen
CALL isra
RETI

ORG 013h; ISR um Spieler nach links zu bewegen
CALL move_player_left; bewege Spieler nach links
RETI

init:
MOV TMOD, #10h; timer 1 auf 16-bit modus

; Interrupts frei schalten:
SETB EX0        ; externes Interrupt 0 freischalten
SETB EX1        ; externes Interrupt 1 freischalten
SETB IT0        ; externes Interrupt 0 auf Taktflanke setzen
SETB IT1        ; externes Interrupt 1 auf Taktflanke setzen
SETB EA         ; allgemeine Interrupt Freigabe

CAll schalte_punktmatrix_anzeige_aus; schallte alle LEDS der Punktmatrixanzeige aus

SETB 11h        ; schalte das Game-Over-Bit ein, was dafür sorgt, dass das Spiel nicht läuft und es somit nicht gleich losgeht
loop_zeichne_start_screen:
CALL start_screen; zeige Start-Screen an
JB 11h, loop_zeichne_start_screen; wiederhole Start-Screen wenn das Spiel noch nicht gestartet wurde

start:
CALL count_down   ; zähle von 3 an runter und zeige jede Zahl 1s lang an
CALL init_start_parameter; reset die für das Spiel relevanten Parameter um zu starten
main:             ; start der Spiel schleife
JB 11h, game_over ; springe wenn Spiel verloren
call draw_while_waiting; zeichne das Spielfeld solange, das es zum einen erkenntlich ist und zum anderen das Tempo des Spiels reguliert wird
CALL collision_check; prüfe auf Kollisionen des Balls und agiere im Kollisionsfall entsprechend der Situation (Abprallen, Punkte Erhöhen)
CALL move_ball; bewege den Ball entsprechend der x/y-Geschwindigkeit
CALL check_round; prüfe ob die Runde zu ende ist und handle entsprechend
CALL check_game_over; prüfe ob der Ball außerhalb des Spielfelds ist und setze game over, wenn dem so ist
JMP main; endlosschleife
game_over:
CALL game_over_screen; zeige den game-over-screen an; zeige den game-over-screen an
JB 11h, game_over; wiederhole game-over-screen wenn nicht neu gestartet wurde
CLR 12h; aktiviere Laufschrift um den game-over-screen zuzulassen
JMP start ; ende der Spielschleife

; #########################################################################################################
isra:; Interrupt-Service-Routine für externes Interrupt 0
JB 11h, restart_game  ; starte das Spiel neu wenn grade game over ist
CALL move_player_right ; bewege den Spieler um eins nach rechts
RET
restart_game:
SETB 12h; blockiere laufschrift, damit etwaige Anzeigen unterbrochen werden und das Spiel (neu)gestartet werden kann
CLR 11h; clear game-over-bit
RET

; #########################################################################################################
; zeichne den Spieler die Blöcke und den Ball auf die Punktmatrixanzeige anzeige an P0/P1
draw_game:
; NOTE: R0 wird zum zwischen speichern der Zeile genutzt
; NOTE: 7x5 Punktmatrixanzeige: Spalte low-aktive, Zeile high-aktive (Spalte an P0 Zeile an P1)
; NOTE: es wird zeilenweise durch iteriert und dann immer gleich eine Zeile gesetzt

MOV P1, #1b ; setze erste Zeile für iteration
MOV A, #0h  ; speichere y-Position der ersten Zeile in A um später überprüfen zu können ob sich der Ball in der Zeile befindet

naechste_zeile:
MOV R0, #0h; reset momentane Zeile

JNB P1.0, nach_zeile0_ausgewaehlt ; prüfe ob die momentane Zeile die 1 mit der oberen Blockreihe is und springe falls nicht
MOV R0, 20h                       ; Schalte Zeile an den Blocken der oberen Blockreihe an (da Zeile zuvor 0 muss keine Maskierung verwendet werden)
JMP nach_zeilen_abfragen          ; weitere Überprüfungen der momentane Zeile seine überflüssig
nach_zeile0_ausgewaehlt:

JNB P1.1, nach_zeile1_ausgewaehlt ; prüfe ob die momentane Zeile die 2 mit der unteren Blockreihe is und springe falls nicht
MOV R0, 21h                       ; Schalte Zeile an den Blocken der unteren Blockreihe an (da Zeile zuvor 0 muss keine Maskierung verwendet werden)
JMP nach_zeilen_abfragen          ; weitere Überprüfungen der momentane Zeile seine überflüssig
nach_zeile1_ausgewaehlt:

JNB P1.6, nach_zeilen_abfragen; prüfe ob die momentane Zeile die Zeile mit dem Spieler is und springe falls nicht
; NOTE: ich nutze hier die Adresse für R4 stat R4 weil es keinen dedizierten Befehl dafür gibt, ein Register in ein Register zu schieben
MOV R0, 04h                   ; setze Spieler

nach_zeilen_abfragen:; Sprunglable um Abfragen der Spieler und Block Reihen zu überspringen

; NOTE: es gibt keinen Befehl um den Akku direkt mit einem Register zu vergleichen, daher muss hier die Adresse des Registers genutzt werden
CJNE A, 3h, ueberspringe_ball; überprüfe ob der Ball in der Aktuellen Zeile ist und springe wenn nicht

PUSH 0E0h       ; backup A um später damit wieder arbeiten zu können
CALL hole_bitmuster_fuer_ball_x_position_in_A
ORL 0h, A       ; schalte das Bit an der Stelle des Balls ein
POP 0E0h        ; hole den gespeicherten Akku wieder zurück um ihn verenden zu können

ueberspringe_ball:

XRL 0h, #11111111b; invert R0  bevor es auf P0 geschoben wird, weil die Zeilen low-aktive sind

PUSH 0E0h ; backup A um später damit wieder arbeiten zu können
MOV A, P1 ; hole P1 in Akku um die nächste Zeile auszuwählen
RL A      ; wähle nächste Zeile aus

MOV P0, R0    ; setze Content der momentanen Zeile

; NOTE: warte mindesten 3 Mz sonst ist auf dem ISIS-board nichts zu sehen
NOP; mache 1 Mz lang nichts
NOP; mache 1 Mz lang nichts
NOP; mache 1 Mz lang nichts

MOV P0, #0ffh ; lösche Zeile bevor die nächste Zeile ausgewählt wird, damit die vorherige nicht in die nächste gezeichnet wird
MOV P1, A     ; wähle nächste Zeile auf dem Port

POP 0E0h      ; hole den gespeicherten Akku wieder zurück um ihn verenden zu können
INC A         ; lade den Index der nächsten Zeile in den Akku

; NOTE:  ride meckert, wenn man P1.7 statt 97h nimmt
JNB 097h, naechste_zeile; wenn die Iteration nicht bei der letzten Zeile angelangt ist fahre mit der nächsten Zeile fort
RET

; #########################################################################################################
; bewege den Ball entsprechend seines x/y-Geschwindigkeit eins weiter
move_ball:
; bewege ball in der vertikalen
XCH A, R2; vertausche Akku und Speicher der x-Position des Balls um die Position zu verändern
ADD A, R5; addiere x-Position mit ball x-Geschwindigkeit um die nächste Position zu errechnen
XCH A, R2; vertausche Akku und Speicher der x-Position des Balls zurück
; bewege ball in der horizontalen
JB 10h, bewege_ball_nach_unten; prüfe ob ball sich nach unten bewegt und springe wenn ja
DEC R3; bewege ball nach oben
RET
bewege_ball_nach_unten:
INC R3; bewege ball nach unten
RET

; #########################################################################################################
; prüft auf Kollision des Balls und wenn nötig lasse ihn abprallen, zerstöre Blöcke und erhöhe die Punkte
collision_check:

; Kollisionsabfrage mit Obergrenze .......................................................
; prüfe Kollision mit oberere Wand
JB 10h, ball_kollidiert_nicht_mit_ober_grenze; prüfe ob sich der Ball nach unten bewegt und springe wenn ja, da er dann nicht mit dir Obergrenze kollidiert
CJNE R3, #0h, ball_kollidiert_nicht_mit_ober_grenze; Ball in oberster Reihe? nein dann springe, weil er dan nicht mit der Obergrenze kollidiert
SETB 10h            ; lasse den Ball nach unten fliegen (von oberer Wand abprallen)
JMP collision_check ; prüfe auf weitere Kollisionen
ball_kollidiert_nicht_mit_ober_grenze:

; Kollisionsabfrage mit rechter Wand .....................................................
CJNE R5, #1h, ball_kollidiert_nicht_mit_rechter_wand; prüfe ob Ball nach rechts fliegt, da er nur dann mit der rechten Wand kollidieren kann und springe falls nein
CJNE R2, #4h, ball_kollidiert_nicht_mit_rechter_wand; prüfe ob der Ball in der Spalte neben der rechten Wand ist und springe falls nein
MOV R5, #0ffh; lasse den Ball nach links fliegen, was ihn, da er zuvor nach rechts flog umkehren lässt
JMP collision_check; prüfe auf weitere Kollisionen
ball_kollidiert_nicht_mit_rechter_wand:

; Kollisionsabfrage mit linker Wand ......................................................
CJNE R5, #0ffh, ball_kollidiert_nicht_linker_wand; prüfe ob Ball sich nach links bewegt und springe wenn nicht, da er nur dann mit der linken Wand kollidieren kann
CJNE R2, #0h, ball_kollidiert_nicht_linker_wand; prüfe ob Ball in Reihe neben der linken Wand ist und spring wenn nein
MOV R5, #1h; lasse den Ball nach rechts fliegen, was ihn, da er zuvor nach links flog umkehren lässt
JMP collision_check; prüfe auf weitere Kollision
ball_kollidiert_nicht_linker_wand:

; Kollisionsabfrage für vertikale Kollision mit Blöcken ..................................
; prüfe vertikale Kollision mit Blöcken (mit vertikale ist von der Ober- oder Unterkante gemeint)
CALL get_next_y_position_in_A; errechne die nächste y-Position des Balls und speicher sie in A
ADD A, #20h ; A + 20 um so die dadr der Blockreihe zu erhalten falls der Ball kollidiert und so später Befehle zu sparen
MOV B, #22h ; max index der Blockreihen + 20 + 1
DIV AB      ; für A < B: A/B = 0 (weil ohne Komma gerechnet wird) & Rest (gespeichert in B) = A
JNZ ball_kollidiert_nicht_vertikal_mit_block; springe wenn der Ball nicht als nächstes in einer der Blockreihe wäre
MOV R0, B ; hole Adresse der Blockreihe mit der der Ball als nächstes kollidieren könnte in R0 (diese wahr Rest der Division und ist somit in B)
MOV A, @R0; hole den Content der nächsten Reihe in A
CALL gib_kollisionsmuster; lade Muster in Akku, dass an Stelle des Balls eine 1 hat sofern an dieser Stelle in der Blockreihe eine Block ist sonst 0
JZ ball_kollidiert_nicht_vertikal_mit_block; wenn A = 0 dann kollidiert der Ball nicht springe daher
CPL 10h    ; lasse Spieler umkehren/abprallen
CAll zerstoere_block_an_kollisionsmuster; zerstöre den Block an der stell der 1 im Kollisionsmuster also der mit dem der Ball kollidiert
CALL inc_points; erhöhe den Punktestand des Spiels
JMP collision_check; prüfe auf weitere Kollision
ball_kollidiert_nicht_vertikal_mit_block:

; Kollisionsabfrage für vertikale Kollisionen mit Spieler ................................
; prüfe vertikale Kollision mit Spieler (mit vertikale ist von der Ober- oder Unterkante gemeint (beim Spieler nur mit der Oberkante, da die Unterkante nicht getroffen werden kann))
JNB 10h, ball_kollidiert_nicht_vertikal_mit_spieler; springe wenn sich der Ball nicht nach unten bewege da er nur dann mit dem Spieler kollidieren kann
CJNE R3, #5h, ball_kollidiert_nicht_vertikal_mit_spieler; springe wenn Ball nicht in der Reihe vor dem Spieler
MOV A, R4                           ; hole Position des Spiels in A um auf Kollision zu prüfen
call gib_kollisionsmuster ; lade Muster in Akku, dass an Stelle des Balls eine 1 hat sofern an dieser Stelle ein Block des Spielers ist sonst 0
JZ ball_kollidiert_nicht_vertikal_mit_spieler; wenn A = 0 dann kollidiert der Ball nicht springe daher
CLR 10h             ; drehe y-Richtung des Balls um
CALL asymmetric_bounce_from_player; erhöhe den x-Speed in Richtung der Seite von der der Ball vom Spieler abprallt sofern der maximale x-Speed Betrag nicht erreicht wurde
JMP collision_check; prüfe auf weitere Kollision
ball_kollidiert_nicht_vertikal_mit_spieler:

; ----------------------------------------------------------------------------------------
; beende die Kollisionsabfrage wenn der Ball sich nicht in der Horizontalen bewegt, da die folgenden Abfragen dann nicht erfüllt werden können
CJNE R5, #0h, ball_bewegt_sich_in_der_horizontalen; springe, wenn sich der Ball in der Horizontalen bewegt
RET; beende die Kollisionsabfrage
ball_bewegt_sich_in_der_horizontalen:

; Kollisionsabfrage für seitliche Kollision mit Block ....................................
MOV A, R3   ; move momentane Ball-y-Position in A
MOV B, #2h  ; move y-Position der Unteren Blockreihe(1) + 1 in B
DIV AB      ; für A < B: Ergebnis = 0 & Rest (gespeichert in A) = A
JNZ ball_kollidiert_nicht_seitlich_mit_block; spring wenn A != 0, was bedeutet, dass der Ball nicht in den Blockreihen ist

; berechne die dadr der Blockreihe
MOV A, B    ; A := B (rest der Division = A vor Division)
ADD A, #20h ; addiere die dadr der oberen Blockreihe um so die dadr der tatsächlichen zu erhalten
MOV R0, A   ; schiebe die dadr der Blockreihe in der der Ball ist in R0 für spätere Verwendung

; berechne Kollisionsmuster der nächste x-Position mit der Blockreihe in der sich der Ball befindet
MOV DPTR, #tab_bitmuster_zu_x_position; hole Tabelle für zugehöriges Bitmustern zu x-Position
CALL get_next_x_position_in_A; hole die nächste x-Position des Balls in A
MOVC A, @A+DPTR; hole Bitmustern zur x-Position aus Tabelle (Bitmustern mit 1 an stelle der x-Position)
ANL A, @R0; Undverknüpfung von Bitmustern der x-Position und des Contents der Blockreihe in der der Spieler ist

JZ ball_kollidiert_nicht_seitlich_mit_block; wenn Kollisionsmuster = 0 dann kollidiert der Ballnicht springe daher

CALL zerstoere_block_an_kollisionsmuster ; zerstöre Block an der Stell an der das Kollisionsmuster eine 1 aufweist
CALL invert_x_speed                       ; komplementiere den y-Speed
CALL inc_points                           ; erhöhe den Punktestand
JMP collision_check                      ; prüfe auf weitere Kollisionen
ball_kollidiert_nicht_seitlich_mit_block:

; Kollisionsabfrage für diagonale Kollisionen ............................................
CALL get_next_y_position_in_A; errechnen nächste y-Position des Balls und speicher sie in A
ADD A, #20h; erhalte die Speicherstelle der Reihe an der y-Position, sofern diese in den Blockreihe ist

MOV R0, A  ; move A in R0 für spätere Operationen

MOV B, #22h; schiebe dadr der unteren Blockreihe +1 in B
DIV AB; für A < B: Ergebnis (gespeichert in A) = 0 & Rest (gespeichert in B) = A
JNZ nach_kollision_mit_ecke_eines_blocks; überspringe Kollision mit Ecke eines Blocks sofern sich die nächste y-Position des Balls nicht in einer Blockreihe befinden und somit kein solche Kollision statt finden kann

; erhalte Kollisionsmuster der nächsten y-Position des Balls mit der Blockreihe in der er als nächstes sein wird
CALL get_next_x_position_in_A         ; errechnen die nächste x-Position des Balls und speicher sie in A
MOV DPTR, #tab_bitmuster_zu_x_position; hole Tabelle für zugehöriges Bitmustern zu x-Position
MOVC A, @A+DPTR                       ; hole Bitmustern für nächste y-Position in A
ANL A, @R0                            ; Undverknüpfung von Bitmustern zur nächsten y-Position des Balls und der Blockreihe in der der Ball als nächstes sein würde (Ergebnisse = Kollisionsmuster)

JZ nach_kollision_mit_ecke_eines_blocks   ; wenn Kollisionsmuster = 0 dann hat keine Kollision statt gefunden überspringe daher dann die Kollision
CALL zerstoere_block_an_kollisionsmuster  ; zerstöre Block mit dem der Ball kollidiert
CPL 10h                                   ; invert y-Richtung des Balls
CALL invert_x_speed                       ; kehre den x-Speed um
CALL inc_points                           ; erhöhe die Punkte
JMP collision_check                       ; prüfe auf weitere Kollisionen
nach_kollision_mit_ecke_eines_blocks:

; NOTE: die folgenden Zeilen dienten dazu den Ball von den Ecken des Spielers abprallen zu lassen; sie wurden auskommentiert um das Spiel zu erschweren, da so die Fläche von der der Ball abprallt verkleinert wird; zuvor musste man sich nicht bewegen um alle Bälle abzuwehren
; CJNE R0, #26h, ball_kollidiert_nicht_mit_ecke_des_spielers; prüfe ob nächste y-Position = Reihe des Spielers + 20 (die 20 zur Berechnung der Adresse für Blockreihe) und springe wenn nicht
; CALL get_next_x_position_in_A         ; errechnen die nächste x-Position des Balls und speicher sie in A
; MOV DPTR, #tab_bitmuster_zu_x_position; hole Tabelle um Bitmustern zu x-Position zu zuordnen
; MOVC A, @A+DPTR                       ; hole Bitmustern für nächste x-Position in A
; ANL A, R4                             ; Undverknüpfung von Bitmuster der nächsten Position und der Position des Spielers (Ergebnis = Kollisionsmuster)
; JZ ball_kollidiert_nicht_mit_ecke_des_spielers; springe wenn Kollisionsmuster = 0 weil dann keine Kollision stattfand
; CPL 10h             ; invert ball y-Richtung
; ; NOTE: asymmetrisches bounce from player ist nicht notwendig da der Ball immer diagonale abprallt und somit immer den max x-Speed hat
; CALL invert_x_speed ; kehre x-Speed um
; JMP collision_check ; prüfe auf weitere Kollisionen
; ball_kollidiert_nicht_mit_ecke_des_spielers:
RET

; #########################################################################################################
; erhöhe den x-Speed in Richtung der Seite von der der Ball vom Spieler abprallt
; sofern der maximale x-Speed Betrag nicht erreicht wurde
; die Seite von der der Ball abprallt wird über ein Kollisionsmuster bestimmt das zuvor in A gespeichert wird
asymmetric_bounce_from_player:
MOV B, A  ; schiebe Kollisionsmuster in A um dadurch zu teilen und so die Seite von der der Ball abprallt zu ermitteln
MOV A, R4 ; hole Spielerposition in in A
DIV AB    ; teile Spielerposition durch Kollisionsmuster; Ergebnis = 1 wenn Ball auf der rechten Seite außer wenn der Spieler an den Rändern dann ist es umgekehrt ansonsten Ergebnis > 1

CJNE R4, #10001b, nicht_ausnahme; prüfe auf Ausnahme und springe wenn nicht
CJNE A, #1h, bounce_nicht_rechts_bei_ausnahme; springe wenn Ball auf der linken Seite
CALL verringere_x_speed; verringere x-Speed sofern max. Betrag nicht erreicht wurde
RET
bounce_nicht_rechts_bei_ausnahme:
CALL erhoehe_x_speed; erhöhe x-Speed sofern max. Betrag nicht erreicht wurde
RET
nicht_ausnahme:

CJNE A, #1h, bounce_nicht_links; springe wenn Ball auf der rechten Seite
CALL erhoehe_x_speed; erhöhe x-Speed sofern max. Betrag nicht erreicht wurde
RET
bounce_nicht_links:
CALL verringere_x_speed; verringere x-Speed sofern max. Betrag nicht erreicht wurde
nach_asymmetric_bounce:
RET

; #########################################################################################################
erhoehe_x_speed:; erhöhe x-Speed sofern max. Betrag nicht erreicht wurde
CJNE R5, #1h, erhoehe_x_speed_; prüfe ob der max. x-Speed Betrag erreicht wurde und springe wenn nein
RET; verlasse Unterprogramm da max. x-Speed Betrag erreicht ist
erhoehe_x_speed_:
INC R5; erhöhe x-Speed
RET

; #########################################################################################################
verringere_x_speed:; verringere x-Speed sofern max. Betrag nicht erreicht wurde
CJNE R5, #0ffh, verringere_x_speed_; prüfe ob der max. x-Speed erreicht wurde und springe wenn nein
RET; verlasse Unterprogramm da max. x-Speed Betrag erreicht ist
verringere_x_speed_:
DEC R5; verringere x-Speed
RET

; #########################################################################################################
; R0: Blockreihe
; A: Kollisionsmuster
zerstoere_block_an_kollisionsmuster:; Toggle das Bit an der Stelle, an der das Kollisionsmuster ein 1 hat, was den kollidierten Block de facto zerstört da er an der Stelle der 1 sein muss.
XRL A, @R0; toggle das Bit in der Blockreihe an der Stelle an der der Ball kollidiert
MOV @R0, A; schiebe neuen Content der Reihe (ohne zerstörten Block) zurück in ihre Speicherstelle
RET

; #########################################################################################################
get_next_x_position_in_A:; errechne die nächste x-Position des Balls und speicher sie in A
MOV A, R2; hole x-Position des Balls in A
ADD A, R5; addiere den x-Speed um so die nächste x-Position zu erhalten
RET

; #########################################################################################################
get_next_y_position_in_A:; errechne die nächste y-Position des Balls und speicher sie in A
MOV A, R3           ; move y-Position des Balls in A
; errechne nächste y-Position des Balls
JB 10h, adjust_down  ; springe wenn sich der Ball nach unten bewegt
DEC A               ; verschiebe die y-Position in A um eins nach oben
RET                 ; beende Berechnung der nächsten y-Position da diese erfasst wurde
adjust_down:         ; Sprungmarke um y-Position nach unten zu versetzen
INC A               ; verschiebe die y-Position in A um eins nach unten
RET

; #########################################################################################################
invert_x_speed: ; kehre den x-Speed um
MOV A, R5       ; A := x-Speed (R5) ; um ihn zu Multiplizieren
MOV B, #0ffh    ; B := -1 im Zweierkomplement; um beim Multiplizieren den x-Speed umzukehren
MUL AB          ; multipliziere A mit B
MOV R5, A       ; schiebe das Ergebnis zurück in die Speicherstelle des x-Speed (R5)
RET

; #########################################################################################################
; holt ein Bitmuster für die Ball-x-Position in A, welches eine Reihe des Spielfelds mit einer 1 an Stelle des Balls repräsentiert.
hole_bitmuster_fuer_ball_x_position_in_A:
; NOTE: dieses Unterprogramm zu nutzen verschwändet an der ein oder anderen Stell 2 Mz, da der DPTR nicht immer neu gesetzt werden muss (z.B bei Schleifen) aber die sind zu vernachlässigen, da sich der Geschwindigkeitsverlust in Grenzen hält und es schafft besser lesbaren und änderbaren Code
MOV DPTR, #tab_bitmuster_zu_x_position; hole Tabelle mit den Bitmustern für die x-Position des Balls
MOV A, R2                             ; lade Index für Bitmustern in A
MOVC A, @A+DPTR                       ; hole das Bitmustern aus der Tabelle
RET

; #########################################################################################################
check_game_over:    ; überprüfe ob das Spiel verloren wurde und setze game-over-bit wenn ja
MOV A, R3           ; y-Position des Balls in A -> zum arbeiten
MOV B, #7h          ; max. erlaubte y-Position + 1 in B für Rechnung
DIV AB              ; 0 für A (bzw R3) < 7 (weil ohne Komma gerechnet wird)
JZ game_is_not_over ; spring wenn A = 0 also wenn R3 größer 7 war
SETB 11h            ; setze game over flag
game_is_not_over:
RET

; #########################################################################################################
; Erzeuge Kollisionsmuster
; A: der Inhalt der Reihe, von der Geprüft werden soll ob der Ball mit ihr kollidiert
gib_kollisionsmuster:; A = 0 wenn der Ball nicht kollidiert; sonst A = reihe mit einer 1 an der Kollisionsstelle
PUSH 0h   ; backup R0 um Nebeneffekte zu vermeiden
MOV R0, A ; schiebe R0 in A um den Inhalt später weiter zu verwenden
call hole_bitmuster_fuer_ball_x_position_in_A; hole ein Bitmuster aus einer Tabelle, das an der Position des Balls eine 1 hat
ANL A, R0 ; A & R0 = Kollisionsmuster; da nur die Bits 1 bleiben, die in A und R0 1 sind
POP 0h    ; hole backup von R0 zurück um Nebeneffekte zu vermeiden
RET

; #########################################################################################################
move_player_left: ; bewegt den Spieler um eins nach links
XCH A, R4 ; tausche A und R4 dadurch kann mit dem Inhalt von R4, der nun in A ist gearbeitet werden und A bleibt in R0 erhalten
RR A      ; verschiebe Spieler nach links
JNB 0E7h, spieler_nicht_links_aus_Zeile; prüfe ob der Spieler links aus dem Spielfeld bewegt wurde und springe wenn nicht
; lasse den Spieler von der anderen Seite zurück ins Spiel kommen
SETB 0E4h ; setze das Bit an der rechten Seite des Spielfelds
CLR 0E7h  ; lösche das Bit, das das Spielfeld an der linken Seite überschreitet
spieler_nicht_links_aus_Zeile:
XCH A, R4 ; tausche A und R4 zurück; A wird wieder hergestellt und der neue Wert für die Spielerposition wird in ihre anbestimmte Speicherstelle geschrieben RET
RET

; #########################################################################################################
move_player_right: ; bewegt den Spieler um eins nach rechts
XCH A, R4 ; tausche A und R4 dadurch kann mit dem Inhalt von R4, der nun in A ist gearbeitet werden und A bleibt in R0 erhalten
RL A      ; verschiebe Spieler nach rechts
JNB 0E5h, spieler_nicht_rechts_aus_Zeile; prüfe ob der Spieler rechts aus dem Spielfeld bewegt wurde und springe wenn nicht
; lasse den Spieler von der anderen Seite zurück ins Spiel kommen
SETB 0E0h ; setze das Bit an der linken Seite des Spielfelds
CLR 0E5h  ; lösche das Bit, das das Spielfeld an der rechen Seite überschreitet
spieler_nicht_rechts_aus_Zeile:
XCH A, R4 ; tausche A und R4 zurück; A wird wieder hergestellt und der neue Wert für die Spielerposition wird in ihre anbestimmte Speicherstelle geschrieben
RET

; #########################################################################################################
start_screen:; zeige auf Punktmatrixanzeige an, was angezeigt werden soll, bevor das Spiel gestartet wurde

; Laufschrift: arkanoid
MOV DPTR, #tab_zeichen_a; wähle das als nächstes anzuzeigende Zeichen
CALL display_char
CALL speichere_zeichen_tabelle
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_k; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_a; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_n; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_o; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_i; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_d; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: druecke
MOV DPTR, #tab_zeichen_d; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_c; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_k; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: interrupt
MOV DPTR, #tab_zeichen_i; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_n; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_p; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: 0
MOV DPTR, #tab_zeichen_0; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: um
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_m; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: fortzufahren
MOV DPTR, #tab_zeichen_f; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_o; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_z; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_f; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_a; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_h; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_n; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
RET

; #########################################################################################################
game_over_screen:; zeige auf Punktmatrixanzeige an, was angezeigt werden soll, wenn das Spiel verloren wurde

; Laufschrift: game
MOV DPTR, #tab_zeichen_g; wähle das als nächstes anzuzeigende Zeichen
CALL display_char
CALL speichere_zeichen_tabelle
MOV DPTR, #tab_zeichen_a; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_m; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: over
MOV DPTR, #tab_zeichen_o; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_v; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: druecke
MOV DPTR, #tab_zeichen_d; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_c; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_k; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: interrupt
MOV DPTR, #tab_zeichen_i; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_n; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_p; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: 0
MOV DPTR, #tab_zeichen_0; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: um
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_m; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: fortzufahren
MOV DPTR, #tab_zeichen_f; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_o; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_t; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_z; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_u; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_f; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_a; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_h; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_r; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_e; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen
MOV DPTR, #tab_zeichen_n; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

; laufschrift: Leerzeichen
MOV DPTR, #tab_zeichen_space; wähle das als nächstes anzuzeigende Zeichen
CALL laufschrift_zeichen_transition; erzeuge transition vom zuletzt angezeigtem zum als nächstes angezeigten Zeichen

RET

; #########################################################################################################
; zeichnet das Spielfeld solange
draw_while_waiting:
MOV R0, 6h    ; setze Wiederholung der äußeren Schleife auf die Spielgeschwindigkeit
drawing_loop_a:
PUSH 0h       ; backup R0 um den Wert später weiter zu verwenden
MOV R0, #0A0h ; setze Wiederholung der Inneren Schleife

drawing_loop_b:
PUSH 0h       ; backup R0 um den Wert später weiter zu verwenden
CALL draw_game; zeichne das Spielfeld (Blöcke, Spieler, Ball)
POP 0h        ; hole den Wert von R0 zurück in R0
DJNZ R0, drawing_loop_b; verringere R0 und wiederhole innere Schleife wenn R0 != 0

POP 0h        ; hole die verbliebene Wiederholungsanzahl der äußeren Schleife wieder zurück in R0
DJNZ R0, drawing_loop_a; verringere R0 und wiederhole äußere Schleife wenn R0 != 0
RET

; #########################################################################################################
; prüfe ob die momentane Runde zu ende ist, also all Blöcke Zerstört sind
; und starte eine neue, wenn sie vorbei ist indem:
; die Blöcke neu initialisiert werden so ballt der Ball aus den Blockreihen draußen ist
check_round:
MOV A, 20h                    ; lade Blöcke der oberen Reihe in A
CJNE A, #0h, ende_check_round ; prüfe ob alle Blöcke der oberen Reihe zerstört sind und springe wenn nicht
MOV A, 21h                    ; lade Blöcke der zweiten Reihe in A
CJNE A, #0h, ende_check_round ; prüfe ob alle Blöcke der zweiten Reihe zerstört sind und springe wenn nicht
MOV A, R3                     ; Ball-y-Position in A
MOV B, #2h                    ; max. index der Blockreihen + 1 in B
DIV AB                        ; für B > A: Ergebnis = 0
CJNE A, #0h, new_round        ; springe wenn Ball nicht in den Blockreihen
RET                           ; beende das Unterprogramm da der Ball in den Blockreihen
new_round:
MOV 20h, #11111b              ; setze die Blöcke der oberen Reihe neu
MOV 21h, #11111b              ; setze die Blöcke der zweiten Reihe neu
CALL inc_gamespeed            ; erhöhe Spielgeschwindigkeit
ende_check_round:
RET

; #########################################################################################################
inc_gamespeed:; erhöhe Spielgeschwindigkeit
CJNE R6, #3h, ball_hat_nicht_max_geschwindigkeit; teste ob max. Speed erreicht und springe wenn nein
RET; beende Unterprogramm
ball_hat_nicht_max_geschwindigkeit:
DEC R6; erhöhe Geschwindigkeit in dem die Anzahl der Wiederholung der Schleife in der das Spielfeld immer wieder gezeichnet wird verringert wird
RET; beende Unterprogramm

; #########################################################################################################
inc_points:; Erhöht die Punkte des Spielers. Wird nach Zerstören eines Blocks aufgerufen, um die Belohnung in Form von Punkten an den Spieler zu vergeben.
; TODO: erhöhe Punkte in Abhängigkeit von der momentanen Runde
PUSH 0E0h ; speicher backup des Akkus im Stack
MOV A, P2 ; hole Punkte in den Akku um anschließend damit rechnen zu können
INC A     ; erhöhe die Punkte um eins
MOV P2, A ; schreibe die resultierenden Punkte wieder in ihre anbestimmte Speicherstelle
POP 0E0h  ; lade back up des Akkus aus dem Stack
RET

; #########################################################################################################
; initialisiert die Parameter für den Spielstart
init_start_parameter:
CLR 11h         ; setze game over auf falsch
CLR 12h         ; erlaube laufschrift
MOV P2, #0h     ; setze Punkte auf 0

; initialisiere Blöcke
MOV 20h, #11111b; Blockreihe 0 ein schalten
MOV 21h, #11111b; Blockreihe 1 ein schalten

; init Ball: ---------------------------
SETB 10h        ; Ball soll sich am Anfang nach unten bewegen
MOV R5, #00h    ; x-Speed so setzen, das er sch senkrecht bewegt
MOV R4, #00110b ; setze start Position des Spielers
; sitze Ball in die Mitte des Spielfelds:
MOV R2, #02d    ; richte den Ball in der Vertikalen mittig aus
MOV R3, #03d    ; richte den Ball in der Horizontalen mittig aus
MOV R6, #7d   ; Spielgeschwindigkeit je niedriger desto schnelle sofern > 0
RET

; #########################################################################################################
; Laufschrift-Transition zwischen dem zuvor angezeigtem Zeichen und dem nächsten, durch den DTPR ausgewählten Zeichen
laufschrift_zeichen_transition:
JB 12h, ende_laufschrift_zeichen_transition; wenn Laufschrift blockiert ist, dann führe das Unterprogramm nicht aus

MOV R0, #1h; R0 := Anzahl der Spalten des nächsten Zeichens, die angezeigt werden

loop_schiebe_zeichen:
SETB TR1; starte Timer 1
MOV R1, #6h; setze Schleifenzähler (wird jede iteration runtergezählt bis Null)
aeussere_wieder_zeichen_schleife_laufschrift:
JB 12h, ende_laufschrift_zeichen_transition; wenn Laufschrift blockiert ist, dann unterbrochen die Schleife
PUSH 1h; backup Schleifenzähler(R1)
CALL lade_timer_werte_fuer_display_time; lade Timer-Start-Werte

innere_wieder_zeichen_schleife_laufschrift:

CALL schalte_punktmatrix_anzeige_aus ; schalte Portmatrix-Anzeige aus
MOV P0, #11101111b; wähle letzte Spalte aus

; überspringe das Zeichnen der Spalten des neuen Zeichens, wenn in der ersten Iteration, da aufgrund der leeren Spalte zwischen der Zeichen erst noch keine Spalte des neuen Zeichens gezeichnet werden muss
CJNE R0, #1h, zeichne_neue_spalten; springe zum zeichnen der neuen Spalten, wenn R0 != 1
JMP nach_zeichnen_der_neuen_spalten; überspringe das zeichnen der neuen Spalten
; ........... zeichne Spalten des neuen Zeichens
zeichne_neue_spalten:
MOV A, R0; A:= R0 (Anzahl der Spalten des nächsten Zeichens, die angezeigt werden)
loop_neue_zeichen_spalten:
PUSH 0E0h; lade backup von A in den Stack

DEC A; A - 1 um die passende Stelle in der Tabelle zu erhalten
MOVC A, @A+DPTR; hole den wert an der Stell in A aus der Tabelle
MOV P1, A; lade den Wert der Spalten in P1 und zeige sie so an

MOV A, P0; hole P0, worüber die Spalte selektiert wird, in den Akku um die nächste wählen zu können
RR A; lasse das Bitmuster und somit die Null in ihm eins weiter wandern(erzeuge Bitmuster um nächste Spalte auszuwählen)
MOV P1, #0h; Schalte die Spalte aus um zu verhindern, dass die vorherige Spalte in die nächste Geschrieben wird
MOV P0, A; lade den Wert für die Ansteuerung der nächsten Zeile in P0 um sie so auszuwählen

POP 0E0h; hole backup von A zurück in A
DJNZ 0E0h, loop_neue_zeichen_spalten; zähle A runter und Springe wenn 0 nicht erreicht wurde
nach_zeichnen_der_neuen_spalten:

; ........... zeichne leere Spalte zwischen Zeichen
MOV A, P0; hole P0, worüber die Spalte selektiert wird, in den Akku um die nächste wählen zu können
RR A; lasse das Bitmuster und somit die Null in ihm eins weiter wandern(wähle nächste Spalte)
MOV P1, #0h; Schalte die Spalte aus um zu verhindern, dass die letzte Spalte in die nächste Geschrieben wird
MOV P0, A; lade den Wert für die Ansteuerung der nächsten Zeile in P0 um sie so auszuwählen
; ...........

; überspringe das Zeichnen der Spalten des letzten Zeichens, wenn in der Letzten Iteration, da dann keine Spalten zum zeichnen über sind
CJNE R0, #4h, zeichne_alte_spalten; prüfe ob in der letzten Iteration und springe wenn nicht
JMP ueberspringe_zeichne_alte_spalten; überspringe das Zeichnen der alten Spalten
; ........... Zeichne Spalten des alten/letzten Zeichens
zeichne_alte_spalten:
MOV R1, #27h; Lade die letzte(höchste) der Adressen, in die die Tabelle des letzten Zeichens gespeichert wurde in R1. Also die Adresse der rechtesten Spalte des alten Zeichens
loop_zeige_rest_des_alten_buchstabens:
MOV P1, @R1; schiebe den Wert, der in der Adresse, die in R1 steht, in P1
DEC R1; R1 - 1 (hole nächste Adresse)
MOV A, P0; hole P0, worüber die Spalte selektiert wird, in den Akku um die nächste wählen zu können
RR A; lasse das Bitmuster und somit die Null in ihm eins weiter wandern(wähle nächste Spalte)
MOV P1, #0h; Schalte die Spalte aus um zu verhindern, dass die letzte Spalte in die nächste Geschrieben wird
MOV P0, A; lade den Wert für die Ansteuerung der nächsten Zeile in P0 um sie so auszuwählen
JB P0.7, loop_zeige_rest_des_alten_buchstabens; springe, wenn P0.7 = 1; breche ab, wenn die Null, die die Spalte auswählt, die auswählbaren Spalten übergangen hat
; ...........
ueberspringe_zeichne_alte_spalten:

JNB TF1, innere_wieder_zeichen_schleife_laufschrift
CLR TF1; clear Timer-Flag
POP 1h; hole backup des Schleifenzählers zurück in R1
DJNZ R1, aeussere_wieder_zeichen_schleife_laufschrift; zähle Schleifenzähler runter und wieder hole Schleife wenn 0 nicht erreicht wurde
CLR TR1; deaktiviere Timer

INC R0; erhöhe die Zahle der zu zeigenden Spalten des nächsten Zeichens um die Schrift eins weiter zu schieben
CJNE R0, #5h, loop_schiebe_zeichen; springe wenn noch nicht die ersten vier Spalten des nächsten Zeichens angezeigt wurden

CALL display_char; zeichne das komplette nächste Zeichen, da dafür ja noch ein Schritt fehlt
CALL speichere_zeichen_tabelle; speichere das nächste Zeichens als nun mehr Letztes

RET

ende_laufschrift_zeichen_transition:
CLR TR1; deaktiviere Timer für den fall, das es nach einem Abbruch noch nicht passiert ist
CLR TF1; clear Timer-Flag
RET

; #########################################################################################################
schalte_punktmatrix_anzeige_aus:; schalt alle LEDS der Punktmatrix-Anzeige aus
MOV P0, #0h; wähle alle Spalten
MOV P1, #0h; lade eine leere Spalte in alle ausgewählten Spalten
RET

; #########################################################################################################
;speichert die Werte einer fünfspalteigen, ausgewählten Tabelle für die Darstellung von Zeichen in 23h
;bis 27h um auch bei einer anderen ausgewählten Tabelle noch Zugriff auf die Werte zu haben
speichere_zeichen_tabelle:
MOV A, #0h      ; initialisiere Iterator
MOV R0, #23h    ; erste Adresse, in die etwas Geschrieben werden soll

loop_save_char_tab:
PUSH 0E0h       ; backup Iterator
MOVC A, @A+DPTR ; hole wert aus Tabelle
MOV @R0, A      ; schreibe A in Adresse, die in R0 gespeichert ist
POP 0E0h        ; hole backup des Iterators zurück in den Akku
INC A           ; erhöhe Iterator
INC R0          ; hole nächste zu speichernde Adresse
CJNE A, #05h, loop_save_char_tab; wieder hole die Schleife für Iterator in 0 bis inklusive 4
RET

; #########################################################################################################
lade_timer_werte_fuer_display_time:; setzt die Timer-Start-Werte für das anzeigen von Laufschrift
MOV TH1, #88h
MOV TL1, #88h
RET

; #########################################################################################################
display_char:; zeige Zeichen aus Tabelle solange an, dass man es auch lesen kann
CALL lade_timer_werte_fuer_display_time
SETB TR1; schalte timer 1 an

MOV R0, #6h; setze Schleifenzähler

loop_display_char_timer:; start der Schleife in der der Timer immer wieder runter läuft und während der das Zeichen angezeigt wird
JB 12h, end_display_char; breche Unterprogramm ab, wenn Laufschrift blockiert ist

PUSH 0h; backup R0/Schleifenzähler
loop_display_char:
CALL draw_char_from_tab; zeige Zeichen aus Tabelle an
JNB TF1, loop_display_char; wiederhole wenn Timer noch nicht abgelaufen ist
CLR TF1; clear Timer-Flag
POP 0h; hole backup vom Schleifenzähler zurück in R0
DJNZ R0, loop_display_char_timer; verringere R0/Schleifenzähler und wiederhole, wenn dieser noch nicht null

end_display_char:
CLR TR1; schalte Timer 1 aus
RET

; #########################################################################################################
; zeige ein Zeichen aus einer ausgewählten Zeichentabellen auf der Punktmatrixanzeige an
draw_char_from_tab:
CALL schalte_punktmatrix_anzeige_aus
MOV P0, #11111110b; wähle erste (linkeste) Spalte aus
MOV A, #0h        ; initialisiere Iterator

loop_char_columns:
PUSH 0E0h       ; backup Iterator für spätere Verwendung
MOVC A, @A+DPTR ; hole Wert an Stelle des Iterators aus der ausgewählten Tabelle
MOV P1, A       ; schreibe Wert in ausgewählte Spalte

MOV A, P0   ; hole P0, worüber die Spalte selektiert wird, in den Akku um die nächste wählen zu können
RL A        ; lasse das Bitmuster und somit die Null in ihm eins weiter wandern(wähle nächste Spalte)
MOV P1, #0h ; Schalte die Spalte aus um zu verhindern, dass die letzte Spalte in die nächste Geschrieben wird
MOV P0, A   ; lade den Wert für die Ansteuerung der nächsten Zeile in P0 um sie so auszuwählen

POP 0E0h    ; hole backup des Iterators zurück in A
INC A       ; erhöhe Iterator
CJNE A, #05h, loop_char_columns; wiederhole Schleife wenn Iterator != 5
RET

; #########################################################################################################
; zähle von drei an runter und zeige die Zeichen auf der Punktmatrixanzeige
count_down:
MOV DPTR, #tab_zeichen_3        ; wähle erste anzuzeigende Zahl bzw deren Tabelle um sie anzeigen zu können
CALL zeige_zeichen_eine_sekunde ; zeige das Zeichen hier die Zahl 3 eine Sekunde lang an
MOV DPTR, #tab_zeichen_2        ; wähle nächste anzuzeigende Zahl bzw deren Tabelle um sie anzeigen zu können
CALL zeige_zeichen_eine_sekunde ; zeige das Zeichen hier die Zahl 2 eine Sekunde lang an
MOV DPTR, #tab_zeichen_1        ; wähle nächste anzuzeigende Zahl bzw deren Tabelle um sie anzeigen zu können
CALL zeige_zeichen_eine_sekunde ; zeige das Zeichen hier die Zahl 1 eine Sekunde lang an
RET

; #########################################################################################################
  ; zeige ein Zeichen 1s lang an
  zeige_zeichen_eine_sekunde:
; Rechnung:
; 50.000Mz*20 = 1.000.000Mz ^= 1.000.000µs = 1s
; 50000d = C350h
; 10000h - C350h = 3CB0h
; -> high-bit = 3Ch; low-bit = B0h; Iterator = 20d
; NOTE: durch die Simulation braucht es länger als eine Sekunde

MOV R0, #20d; initialisiere Iterator

loop_display_char_eine_s:
; setze Timer-Start-Werte  für Timer 1
MOV TH1, #3Ch
MOV TL1, #0B0h
; MOV TH1, #0ffh
; MOV TL1, #0ffh

CLR TF1 ; lösche Timer-Flag des Timers 1
SETB TR1; starte Timer 1

PUSH 0h; speicher Backup von R0 auf Stack
loop_display_char_warte_auf_timer:
CALL draw_char_from_tab; zeige das Zeichen auf der Punktmatrixanzeige an
JNB TF1, loop_display_char_warte_auf_timer; wiederhole die Schleife/zeige das Zeilen erneut an, wenn der Timer noch nicht abgelaufen ist
POP 0h; hole Backup von R0 vom Stack in R0

DJNZ R0, loop_display_char_eine_s
CLR TF1; lösche Timer-Flag für Timer 1
CLR TR1; stop Timer 1

RET
; #########################################################################################################

; Tabellen:
tab_bitmuster_zu_x_position: DB 00001b, 00010b, 00100b, 01000b, 10000b

; Tabellen für Zeichen, die auf der Punktmatrix-Anzeige angezeigt werden können (Zeichentabellen)
; NOTE: es werden nicht all Buchstaben verwendet aber ich habe mich dazu entschieden für all Tabellen anzulegen, damit man ohne neue anlegen zumüssen alles auf die Anzeige schreiben kann; dies sorgt dafür, dass das Programm leichter erweiterbar ist und es est auch genügent Speicherplatz vorhanden und somit kein Schanden
tab_zeichen_a: DB 1111110b, 0001001b, 0001001b, 0001001b, 1111110b
tab_zeichen_b: DB 1111111b, 1001001b, 1001001b, 1001001b, 0110110b
tab_zeichen_c: DB 0111110b, 1000001b, 1000001b, 1000001b, 0100010b
tab_zeichen_d: DB 1111111b, 1000001b, 1000001b, 1000001b, 0111110b
tab_zeichen_e: DB 1111111b, 1001001b, 1001001b, 1001001b, 1000001b
tab_zeichen_f: DB 1111111b, 0001001b, 0001001b, 0001001b, 0000001b
tab_zeichen_g: DB 0111110b, 1000001b, 1001001b, 1001001b, 0110010b
tab_zeichen_h: DB 1111111b, 0001000b, 0001000b, 0001000b, 1111111b
tab_zeichen_i: DB 1000001b, 1000001b, 1111111b, 1000001b, 1000001b
tab_zeichen_j: DB 0110000b, 1000000b, 1000000b, 1000000b, 0111111b
tab_zeichen_k: DB 1111111b, 0001000b, 0010100b, 0100010b, 1000001b
tab_zeichen_l: DB 1111111b, 1000000b, 1000000b, 1000000b, 1000000b
tab_zeichen_m: DB 1111111b, 0000010b, 0000100b, 0000010b, 1111111b
tab_zeichen_n: DB 1111111b, 0000100b, 0001000b, 0010000b, 1111111b
tab_zeichen_o: DB 0111110b, 1000001b, 1000001b, 1000001b, 0111110b
tab_zeichen_p: DB 1111111b, 0001001b, 0001001b, 0001001b, 0000110b
tab_zeichen_q: DB 0111110b, 1000001b, 1010001b, 1100001b, 1111110b
tab_zeichen_r: DB 1111111b, 0001001b, 0011001b, 0101001b, 1000110b
tab_zeichen_s: DB 0100110b, 1001001b, 1001001b, 1001001b, 0110010b
tab_zeichen_t: DB 0000001b, 0000001b, 1111111b, 0000001b, 0000001b
tab_zeichen_u: DB 0111111b, 1000000b, 1000000b, 1000000b, 0111111b
tab_zeichen_v: DB 0000111b, 0011000b, 1100000b, 0011000b, 0000111b
tab_zeichen_w: DB 0111111b, 1000000b, 0111000b, 1000000b, 0111111b
tab_zeichen_x: DB 1000001b, 1100110b, 0011000b, 1100110b, 1000001b
tab_zeichen_y: DB 0000001b, 0000110b, 1111000b, 0000110b, 0000001b
tab_zeichen_z: DB 1000001b, 1100001b, 1011001b, 1000111b, 1000001b
tab_zeichen_space: DB 0000000b, 0000000b, 0000000b, 0000000b, 0000000b

tab_zeichen_0: DB 0111110b, 1010001b, 1001001b, 1000101b, 0111110b
tab_zeichen_1: DB 1000000b, 1000100b, 1000010b, 1111111b, 1000000b
tab_zeichen_2: DB 1000010b, 1100001b, 1010001b, 1001010b, 1000100b
tab_zeichen_3: DB 0100010b, 1000001b, 1001001b, 0101010b, 0010100b

END
