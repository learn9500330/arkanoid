# Arkonnoid

![](./DocRes/thumbnail.png)

## Beschreibung des Programms:

Es handelt sich hierbei um eine Implementation eines Arkonnoid oder Bricks ähnlichen Spiels. Dieses wird
auf einer 7x5 Punktmatrixanzeige dargestellt. Ferner gibt es noch eine Punkteanzeige für die Punkte des
Spielers.
Der Spieler in der untersten Reihe kann über die externen Interrupts Null und Eins nach rechts
bzw. links verschoben werden.
In den obersten beiden Reihen der insgesamt 7 Reihen befinden sich zu beginn Blöcke, die man zerstören
kann indem man den Ball dagegen prallen lässt. Für jeden zerstörten Block erhält man Punkte.
Der Spieler in der untersten Reihe ist zwei Blöcke breit. Wenn der Ball ihn trifft prallt er von ihm ab.
Trifft er dabei den linken Block so wird seine Geschwindigkeit nach links hin um eins erhöht sofern er
nicht bereits die maximal zulässige Geschwindigkeit von 1 Blöcken pro Zyklus in diese Richtung besitzt.
Prallt er von dem rechten Block ab gilt das selbe in komplementärer Ausführung. Der Ball prallt nicht ab,
wenn der schräg auf die Kante des Spielers trifft. Dies hat den Zweck das Spiel schwerer zu gestalten,
somit die Fläche von der er abprallen kann reduziert wird. Ohne diese Maßnahme ergäbe sich die
Möglichkeit den Spieler so zu plazieren, dass der Ball in jedem sich ergebenen Fall vom aus dem Spielfeld
fliegen abgehalten wird. In der Vertikalen kann sich der Ball nur mit einer Geschwindigkeit von
betragsmäßig 1 bewegen. Wenn der Spieler links oder rechts aus dem Spielfeld geht dan kommt er von der
anderen Seite wieder rein. Man hat das Spiel dann verloren, wenn der Ball an der untersten Reihe vorbei
aus dem Spielfeld fliegt. Von allen anderen Reihen Prallt der Ball ab. Zerstört man alle Blöcke kommt man
in die nächste Runde und die Blöcke werden neu plaziert. Ferner erhöht sich mit jeder Runde die
Geschwindigkeit des Spiels. Pro zerstörten Block bekommt man einen Punkt. Die Punkte werden mit zwei
Ziffern im Hexadezimalsystem dargestellt. Dem entsprechend get es nur bis 256 und danach fängt es wieder
von 0 an. Sollte man so weit kommen muss man sich halt merken, dass es übergelaufen ist.

Du findest nähere Beschreibungen sowohl zur Funktionsweise als auch zur Implementierung in <a href="./Arkanoid_Dokumentation.pdf">Arkanoid_Dokumentation.pdf</a>.

**NOTE:** Leider habe ich den Code nur in einer Simulation (mit ISIS) ausgeführt. Ich weiß also nicht, wie er sich auf einem richtigen 8051 verhält.
